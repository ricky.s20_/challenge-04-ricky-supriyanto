class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.dateButton = document.getElementById("date");
    this.timeButton = document.getElementById("time");
    this.findButton = document.getElementById("find");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    this.findButton.addEventListener("click", ()=>{this.filterA()});
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  //filter penumpang
  filterA = () => {
    
    // console.log("Berhasil")
    const user= document.getElementById("user");
    const date= document.getElementById("date");
    const available = document.getElementById("available")
    
    // ambil data tahun bulan tanggal
   

    let child = this.carContainerElement.firstElementChild;
    //  console.log(dateInput[0],dateInput[1],dateInput[2])
    // console.log(availableat.value)
    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
    Car.list.forEach((car) => {
      const month = [
        // array of month
       "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agust", "Sept","Okt", "Nov", "Des"
      ];
      const dateInput = date.value.split("-");  // [1] itu bulan , [0] tahun, [2] tanggal  

      //ambil data dari car.example.js lalu ubah ke bentuk string
      const dateData = car.availableAt.toString();
      const datedataInput = dateData.split(' ')
      // console.log(datedataInput[0],datedataInput[1],datedataInput[2],datedataInput[3])  //[2] tanggal, [1] bulan. [3] tahun
      
      const timeInput = time.value;
      // console.log(timeInput);

      //ambil score time di string date dataInput
      const datatime = datedataInput[4].split(':')
      const timeScore = Number(datatime[0]*60)+Number(datatime[1])
      
      // console.log(timeScore);
      console.log(datatime);

      //cek capacity
      if (car.capacity >= user.value){
        
        // cek available
        if(available.value == 0){
          // cek date
          if(dateInput[2]==datedataInput[2] && month[Number(dateInput[1])-1]== datedataInput[1] && dateInput[0]==datedataInput[3]){
            //cek time
            if(timeInput==0){
              const node = document.createElement("div");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }else if (timeScore>=timeInput){
              const node = document.createElement("div");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          }
          // cek date no input
          else if(dateInput[0]==""){
            //cek time
            if(timeInput==0){
              const node = document.createElement("div");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }else if (timeScore>=timeInput){
              const node = document.createElement("div");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          }
        }else{
          if(car.available== true && available.value == "true"){
            // cek date
            if(dateInput[2]==datedataInput[2] && month[Number(dateInput[1])-1]== datedataInput[1] && dateInput[0]==datedataInput[3]){
              //cek time
              if(timeInput==0){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }else if (timeScore>=timeInput){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }
            }
            // cek date no input
            else if(dateInput[0]==""){
              //cek time
              if(timeInput==0){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }else if (timeScore>=timeInput){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }
            }
          }
          if(car.available== false && available.value == "false"){
            // cek date
            if(dateInput[2]==datedataInput[2] && month[Number(dateInput[1])-1]== datedataInput[1] && dateInput[0]==datedataInput[3]){
              //cek time
              if(timeInput==0){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }else if (timeScore>=timeInput){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }
            }
            // cek date no input
            else if(dateInput[0]==""){
              //cek time
              if(timeInput==0){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }else if (timeScore>=timeInput){
                const node = document.createElement("div");
                node.innerHTML = car.render();
                this.carContainerElement.appendChild(node);
              }
            }
          }
        }
      }
    });
  }

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}

