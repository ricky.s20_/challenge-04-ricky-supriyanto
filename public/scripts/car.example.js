class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card-item">
        <div class="foto">
          <img src="${this.image}" alt="${this.manufacture}" width="64px">
        </div>
        <div class="desc">
          <p style="text-align:left">${this.manufacture} / ${this.type}</p>
          <p style="text-align:left; font-weight: 700;">Rp ${this.rentPerDay} / hari</p>
          <p style="text-align:left">${this.description}</p>
          <p style="text-align:left">${this.capacity} Orang</p>
          <p style="text-align:left">${this.transmission}</p>
          <p style="text-align:left">Tahun ${this.year}</p>
          <p style="text-align:left">${this.availableAt}</p>
        </div>
        <button type="button" class="btn button-green">Pilih Mobil</button>
      </div>
    `;
  }
}

{/* <p style="text-align:left">${this.availableAt}</p> */}

{/* <div class="card-item">
  <p>id: <b>${this.id}</b></p>
  <p>plate: <b>${this.plate}</b></p>
  <p>manufacture: <b>${this.manufacture}</b></p>
  <p>model: <b>${this.model}</b></p>
  <p>available at: <b>${this.availableAt}</b></p>
  <img src="${this.image}" alt="${this.manufacture}" width="64px">
</div> */}